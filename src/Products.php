<?php
namespace Sani;
use PDO;
use PDOException;

class Products{

    public $conn = '';
    public $brand_id = '';
    public $heading = '';
    public $title = '';
    public $description = '';
    public $price = '';
    public $image = '';
    public $image_small_1 = '';
    public $image_small_2 = '';
    public $image_small_3 = '';
    const PAGINATE_PER_PAGE = 5;

    public function __construct()
    {
        try{
            // session_start();
            $this->conn = new PDO
            ("mysql:host=localhost;dbname=brands","root","admin");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function index()
    {
        $perPage = self::PAGINATE_PER_PAGE;
        if(isset($_GET['page'])){
            $pageNumber = $_GET['page'];
            $offset = ($pageNumber-1) * $perPage;
            $sql = "SELECT * FROM `products` where id_delete = 0 order by id asc limit $perPage OFFSET $offset"; 
        }else{
            $sql = "SELECT * FROM `products` where id_delete = 0 order by id asc limit $perPage";
        }
        $stmt = $this->conn->query($sql);

        $countproductsSql = "SELECT COUNT(*) as totalItem FROM `products`";
        $countproductsStmt = $this->conn->query($countproductsSql);
        $productsCount = $countproductsStmt->fetchColumn();

        return [
            'products' => $stmt->fetchAll(),
            'products_count' => $productsCount
        ];
    }

    public function SetData(array $data = [])
    {
        
        session_start();
        $errors = [];
        if($_FILES['image']['name']){
            //image vadidation
            $imageTmpName = $_FILES['image']['tmp_name'];
            $orginalName = $_FILES['image']['name'];
            $imageSize = $_FILES['image']['size'];

            //image type validation
            $imageExplode = explode('.', $orginalName);
            $imageName = end($imageExplode);
            $imageType = ['png', 'jpg', 'jpeg'];
            if(!in_array($imageName, $imageType)){
                $errors[] = 'Type Required';
            }

            //image size validation
            if($imageSize > 55048576){
                $errors = 'Size Required';
            }
        }

        if($_FILES['image_small_1']['name']){
            //image vadidation
            $imageTmpName1 = $_FILES['image_small_1']['tmp_name'];
            $image1OginalName = $_FILES['image_small_1']['name'];
            $imageSize1 = $_FILES['image_small_1']['size'];

            //image type validation
            $imageExplode1 = explode('.', $image1OginalName);
            $imageName1 = end($imageExplode1);
            $imageType1 = ['png', 'jpg', 'jpeg'];
            if(!in_array($imageName1, $imageType1)){
                $errors[] = 'Type Required';
            }

            //image size validation
            if($imageSize1 > 55048576){
                $errors = 'Size Required';
            }
        }

        if($_FILES['image_small_2']['name']){
            //image vadidation
            $imageTmpName2 = $_FILES['image_small_2']['tmp_name'];
            $image2OrginalName = $_FILES['image_small_2']['name'];
            $imageSize2 = $_FILES['image_small_2']['size'];

            //image type validation
            $imageExplode2 = explode('.', $image2OrginalName);
            $imageName2 = end($imageExplode2);
            $imageType2 = ['png', 'jpg', 'jpeg'];
            if(!in_array($imageName2, $imageType2)){
                $errors[] = 'Type Required';
            }

            //image size validation
            if($imageSize2 > 55048576){
                $errors = 'Size Required';
            }
        }

        if($_FILES['image_small_3']['name']){
            //image vadidation
            $imageTmpName3 = $_FILES['image_small_3']['tmp_name'];
            $image3OrginalName = $_FILES['image_small_3']['name'];
            $imageSize3 = $_FILES['image_small_3']['size'];

            //image type validation
            $imageExplode3 = explode('.', $image3OrginalName);
            $imageName3 = end($imageExplode3);
            $imageType3 = ['png', 'jpg', 'jpeg'];
            if(!in_array($imageName3, $imageType3)){
                $errors[] = 'Type Required';
            }

            //image size validation
            if($imageSize3 > 55048576){
                $errors = 'Size Required';
            }
        }

        //form validation
        if(array_key_exists('brand_id', $data) && !empty($data['brand_id'])){
            $this->brand_id = $data['brand_id'];
        }else{
            $errors[] = 'Brand Id  required';
        }
        if(array_key_exists('heading', $data) && !empty($data['heading'])){
            $this->heading = $data['heading'];
        }else{
            $errors[] = 'heading required';
        }
        if(array_key_exists('title', $data) && !empty($data['title'])){
            $this->title = $data['title'];
        }else{
            $errors[] = 'title required';
        }
        if(array_key_exists('description', $data) && !empty($data['description'])){
            $this->description = $data['description'];
        }else{
            $errors[] = 'description required';
        }
        if(array_key_exists('price', $data) && !empty($data['price'])){
            $this->price = $data['price'];
        }else{
            $errors[] = 'Price required';
        }

        if(count($errors)){
            $_SESSION['errors'] = $errors;
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            //image move
            if($_FILES['image']['name']){
                $imageNameUnic = time().'__'.$orginalName;
                $this->image = $imageNameUnic;
                move_uploaded_file($imageTmpName, '../../assets/image/product/'.$imageNameUnic);
            }

            if($_FILES['image_small_1']['name']){
                $imageNameUnic1 = time().'__'.$image1OginalName;
                $this->image_small_1 = $imageNameUnic1;
                move_uploaded_file($imageTmpName1, '../../assets/image/product/'.$imageNameUnic1);
            }

            if($_FILES['image_small_2']['name']){
                $imageNameUnic2 = time().'__'.$image2OrginalName;
                $this->image_small_2 = $imageNameUnic2;
                move_uploaded_file($imageTmpName2, '../../assets/image/product/'.$imageNameUnic2);
            }

            if($_FILES['image_small_3']['name']){
                $imageNameUnic3 = time().'__'.$image3OrginalName;
                $this->image_small_3 = $imageNameUnic3;
                move_uploaded_file($imageTmpName3, '../../assets/image/product/'.$imageNameUnic3);
            }
            return $this;
        }
        
    }

    public function brand()
    {
        $sql = "SELECT  id, name FROM `brands`";
        $stmt = $this->conn->query($sql);
        $brands = $stmt->fetchAll();
        return ['brands'=>$brands];
        
    }

    public function store()
    {
        try{

        $query ="INSERT INTO products(brand_id, heading, title, price, image, image_small_1, image_small_2, image_small_3, description) VALUES(:brand_id, :heading, :title , :price, :image, :image_small_1, :image_small_2, :image_small_3, :description)";
        $stmt =$this->conn->prepare($query);
        $stmt->execute(array(
        ':brand_id' => $this->brand_id,
        ':heading' => $this->heading,
        ':title' => $this->title,
        ':price' =>(int) $this->price,
        ':image' => $this->image,
        ':image_small_1' => $this->image_small_1,
        ':image_small_2' => $this->image_small_2,
        ':image_small_3' => $this->image_small_3,
        ':description' => $this->description
        ));

        $_SESSION['message'] = 'Successfull Create !';
        header('Location:index.php');

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function show($id)
    {
        $query = "SELECT * FROM products where id=".$id;
        $stmt = $this->conn->query($query);
        return $stmt->fetch();
    }

    public function update($id)
    {
        try{
            
            $sql = 'SELECT * FROM `products` WHERE id='.$id;
            $stmt = $this->conn->query($sql);
            $products = $stmt->fetch();

            $query ="UPDATE products SET brand_id=:brand_id, heading=:heading, 
            title=:title, price=:price, image=:image, image_small_1=:image_small_1, 
            image_small_2=:image_small_2, image_small_3=:image_small_3, 
            description=:description where id = ".$id;

            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':brand_id' => $this->brand_id,
                ':heading' => $this->heading,
                ':title' => $this->title,
                ':price' =>(int) $this->price,
                ':image' => $this->image,
                ':image_small_1' => $this->image_small_1,
                ':image_small_2' => $this->image_small_2,
                ':image_small_3' => $this->image_small_3,
                ':description' => $this->description
            ));

            unlink("../../assets/image/product/".$products['image']);
            unlink("../../assets/image/product/".$products['image_small_1']);
            unlink("../../assets/image/product/".$products['image_small_2']);
            unlink("../../assets/image/product/".$products['image_small_3']);
            $_SESSION['message'] = 'Successfully Updated !';
            header('Location:index.php');

        } catch (PDOException $e){
            echo $e->getMessage();
        }
            
    }

    public function delete($id)
    {
        try {

            $sql = 'SELECT * FROM `products` WHERE id='.$id;
            $stmt = $this->conn->query($sql);
            $products = $stmt->fetch();

            $query ="delete from  where id=".$id;
            $stmt = $this->conn->query($query);
            $stmt ->execute();

            unlink("../../assets/image/product/".$products['image']);
            unlink("../../assets/image/product/".$products['image_small_1']);
            unlink("../../assets/image/product/".$products['image_small_2']);
            unlink("../../assets/image/product/".$products['image_small_3']);
            $_SESSION['message'] = 'Successfull Delete !';
            header('Location:trash.php');
            
        }catch(PDOException $e){
            echo $e->getMessage();
        }     
    }

    public function destroy($id)
    {
        try{
            $query ="UPDATE products SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 1
            ));
            $_SESSION['message'] = 'Successfully Delete !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function trash()
    {
        $query = "SELECT * FROM products where id_delete=1";
        $stmt = $this->conn->query($query);
        return $stmt->fetchAll();
    }

    public function restore($id)
    {
        try{
            $query ="UPDATE products SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 0
            ));

            $_SESSION['message'] = 'Successfully Restore !';
            header('Location:trash.php');

        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    

}

?>