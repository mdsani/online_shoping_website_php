
<!--php code-->
<?php
require_once 'vendor/autoload.php';
use Sani\Brands;
$brand = new Brands;
$productobj = $brand->productdesc();
$product = $productobj['products'];
?>

<section id="shoes" class="my-10">
    <div class="text-center container mt-5 py-5">
      <h2>Our Product</h2>
      <hr class="mx-auto h">
      <p>This is our product</p>
    </div>
    <!--cart section-->
    <div class="row mx-auto container-fluid">
        <?php foreach($product as $products){ ?>
          <div onclick="window.location.href='shopProductDetails.php?id=<?= $products['id'] ?>';" class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/product/<?=  $products['image'] ?>" style="height: 200px; width:100%;" class="mb-5 mt-3" alt="">
            <h5> <?= $products['title'] ?></h5>
            <h4>$<?= $products['price'] ?></h4>
            <button class="buy-btn">Buy Now</button>
          </div>
        <?php } ?>
    </div>
        
 </section>