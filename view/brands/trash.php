<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>

<!--php code-->
<?php
require_once '../../vendor/autoload.php';
use Sani\Brands;
$brand = new Brands;
$brands = $brand->trash();
?>

<!--php error code-->
<?php session_start(); if(isset($_SESSION['message'])){ ?>
<p class="text-center"><?= $_SESSION['message'] ?></p>
<?php }unset($_SESSION['message']); ?>

<!--table-->
<h3 class="text-center">Trash</h3>

<table class="table table-striped container">
  <thead>
    <tr>
      <th scope="col">SL#</th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col" class="text center" style="width: 300px;">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $sl=0; foreach($brands as $brand){ ?>
        <tr>
            <td><?= ++$sl; ?></td>
            <td><?= $brand['name'] ?></td>
            <td><?= $brand['description'] ?></td>
            <td>
                <a class="btn btn-primary" onclick="return confirm('Are you sure want to restore')" href="restore.php?id=<?= $brand['id'] ?>" role="button">Restore</a>
                <a class="btn btn-primary" onclick="return confirm('Are you sure want to delete permanently')" href="delete.php?id=<?= $brand['id'] ?>" role="button">Delete</a>
            </td>
        </tr>
    <?php } ?>
  </tbody>
</table>

<div class="text-center container mb-2">
    <a class="btn btn-primary" href="index.php" role="button">List</a>
</div>

<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>