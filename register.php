
  <!--Header section-->
<?php
include 'layout/header.php';
?>

<!--register main-->
<section class="info" style="width: 500px; margin: auto; margin-top:10px;">
    <h1 class="text-center">Registration form</h1>
    <br>
    <form  action="home.php" method="post">
        <div class="row mb-3">
        <div class="col-md-6 ">
            <label for="inputEmail4" class="form-label">First Name</label>
            <input type="text" name="f_name" class="form-control" id="inputEmail4" placeholder="First Name" required>
        </div>
        <div class="col-md-6 ">
          <label for="inputEmail4" class="form-label">Last Name</label>
          <input type="text" name="l_name" class="form-control" id="inputEmail4" placeholder="Last Name" required>
        </div>
        </div>
        <div class="row mb-3">
        <div class="col-md-6 ">
          <label for="inputEmail4" class="form-label">Email</label>
          <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email" required>
        </div>
        <div class="col-md-6">
          <label for="inputPassword4" class="form-label">Password</label>
          <input type="password" name="pass" class="form-control" id="inputPassword4" placeholder="Password" required>
        </div>
        </div>

      <div class="row mb-3">
      <div class="col-md-12 mb-3">
        <label for="inputAddress" class="form-label">Present Address</label>
        <input type="text" name="presentAddress" class="form-control" id="inputAddress" placeholder="Present Address" required>
      </div>
      <div class="col-md-12">
        <label for="inputAddress2" class="form-label">Permanent Address</label>
        <input type="text" name="permanentAddress" class="form-control" id="inputAddress2" placeholder="Permanent Address" required>
      </div>
      </div>


      <div class="row mb-3">
      <div class="col-md-6">
        <label for="inputCity" class="form-label">City</label>
        <input type="text" name="city" class="form-control" id="inputCity">
      </div>
      <div class="col-md-4">
        <label for="inputState" class="form-label">State</label><br>
        <select class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;" aria-label="Default select example">
          <option selected>Choose... </option>
          <option value="Dhaka">Dhaka</option>
          <option value="Barisal">Barisal</option>
          <option value="Sylhet">Sylhet</option>
        </select>
      </div>
      <div class="col-md-2">
        <label for="inputZip" class="form-label">Zip</label>
        <input type="text" name="zip_code" class="form-control" id="inputZip">
      </div>
      </div>

      <div class="row mb-3">
      <div class="col-md-12">
        Gender :
            
              <input type="radio" checked="checked" value="Male" id="male" name="radio">
              <label for="male"> Male</label>
      
              <input type="radio" id="female"   value="Female" name="radio">
              <label for="female"> Female</label>
            
              <input type="radio" id="other"  value="Other" name="radio">
              <label for="other"> Other</label>
      </div>
      </div>
      <div class="row mb-3">
      <div class="col-md-12">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" id="gridCheck">
          <label class="form-check-label" for="gridCheck">
          Remember username
          </label>
        </div>
      </div>
      </div>

      <div class="row">
      <div class="col-md-12">
        <a href="login.php" class="btn btn-primary ">Login</a>
        <button type="submit" name="sign_up" class="btn btn-primary right-btn">Submit</button>

      </div>
      </div>
      
      
    </form>

  </section>


  <!--Footer section-->
<?php
include 'layout/footer.php';
?>





