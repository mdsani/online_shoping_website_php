
<!--Header section-->
<?php
include 'layout/header.php';
?>

<!--php code-->
<?php
require_once 'vendor/autoload.php';
use Sani\Products;
use Sani\Brands;

$productobj = new Products;
$product = $productobj->show($_GET['id']);

$brand = new Brands;
$brandProducts = $brand->showRecommand($product['brand_id']);
$productbrand = $brandProducts['products'];

?>

<!--cart section-->
    <section class="sproduct  container">   
        <div class="row mt-5">
            <div class="col-lg-5">
                <img src="assets/image/product/<?=  $product['image'] ?>" style="height: 500px;" class="img-fluid w-100 pb-1 im" id="MainImg" alt="">

                <div class="small-img-group">
                    <div class="small-img-col">
                        <img src="assets/image/product/<?=  $product['image'] ?>" width="100%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="assets/image/product/<?=  $product['image_small_1'] ?>" width="100%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="assets/image/product/<?=  $product['image_small_2'] ?>" width="100%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="assets/image/product/<?=  $product['image_small_3'] ?>" width="100%" class="small-img" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h6 style="color: gray;"><?= $product['heading']; ?></h6>
                <h3 class="pb-4"><?= $product['title']; ?></h3>
                <h2><?= $product['price']; ?></h2>
                <select name="" id="" class="my-3">
                    <option value="">Select Size</option>
                    <option value="">Xl</option>
                    <option value="">XXl</option>
                    <option value="">Small</option>
                    <option value="">Large</option>
                </select>
                <input type="number" value="1">
                <button class="buy-btn">Add to Cart</button>
                <h4 class="mt-5 pb-5">Product Details</h4>
                <p><?= $product['description']; ?></p>
            </div>
        </div>

    <!--recommand section-->
    <div class=" container mt- py-5">
          <h2 class="text-center">Recommand Product</h2>
          <div class="d-flex justify-content-center">
          <hr>
          </div>
          <p class="text-center">This is our product</p>
        </div>
    <div class="row mx-auto container-fluid">
        <?php foreach($productbrand as $products){ ?>
          <div onclick="window.location.href='shopProductDetails.php?id=<?= $products['id'] ?>';" class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/product/<?=  $products['image'] ?>" style="height: 200px; width:100%;" class="mb-5 mt-3" alt="">
            <h5> <?= $products['title'] ?></h5>
            <h4>$<?= $products['price'] ?></h4>
            <button class="buy-btn">Buy Now</button>
          </div>
        <?php } ?>
    </div>
    </section>

<!--Footer section-->
<?php
include 'layout/footer.php';
?>