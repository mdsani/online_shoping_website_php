<!--Header section-->
<?php
include 'layout/header.php';
?>

<!--home section-->
<?php
include 'layout/home.php';
?>

<!--feature section-->
<?php
include 'layout/feature.php';
?>

<!--discount section-->
<?php
include 'layout/discount.php';
?>

<!--banner section-->
<?php
include 'layout/banner.php';
?>

<!--product section-->
<?php
include 'view/product/ourproduct.php';
?>

<!--Footer section-->
<?php
include 'layout/footer.php';
?>