
<!--Header section-->
<?php
include 'layout/header.php';
?>

<!--login section-->
<div class="container">
          <div class="mydiv mt-5">
            <div class="right-content">
                <form action="#" class="myform text-center">
                    <header>login account</header>
                    <div class="form-group">
                        <i class="fas fa-envelope"></i>
                        <input type="email" class="myinput" placeholder="Email" id="email" required>
                    </div>
                    <div class="form-group">
                        <i class="fas fa-lock"></i>
                        <input type="password" class="myinput" placeholder="Password" id="password" required>
                    </div>
                    <input type="submit" class="right-btn" value="login">
                    <a href="register.php"><button type="button" class="left-btn">Create Account</button></a>
                </form>
            </div>
          </div>
      </div>

<!--Footer section-->
<?php
include 'layout/footer.php';
?>