<section id="featured" class="my-10">
      <div class="text-center container mt-5 py-5">
        <h2>Our Featured</h2>
        <hr class="mx-auto h">
        <p>This is our product</p>
      </div>
      <div class="row mx-auto container-fluid">
          <div class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/feature/1.png" class="img-fluid mb-3 mt-3" style=" height:200px;" alt="">
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <h5 class="p-name">Cannon</h5>
            <h4 class="p-price">$........</h4>
            <button class="buy-btn">Buy Now</button>
          </div>
          <div class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/feature/2.jpg" class="img-fluid mb-3 mt-3" style=" height:200px;"  alt="">
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <h5 class="p-name">macbook</h5>
            <h4 class="p-price">$........</h4>
            <button class="buy-btn">Buy Now</button>
          </div>
          <div class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/feature/3.jpg" class="img-fluid mb-3 mt-3"style=" height:200px;" alt="">
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <h5 class="p-name">Xiaomi</h5>
            <h4 class="p-price">$........</h4>
            <button class="buy-btn">Buy Now</button>
          </div>
          <div class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
            <img src="assets/image/feature/4.jpg" class="img-fluid mb-3 mt-3" style=" height:200px;" alt="">
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <h5 class="p-name">Oppo</h5>
            <h4 class="p-price">$........</h4>
            <button class="buy-btn">Buy Now</button>
          </div>
      </div>
   </section>