<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online</title>
    <!--Fontawesome cdn-->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!--main css-->
    <link rel="stylesheet" href="./assets/css/style.css">
</head>
<body>
<!--php code-->
<!--php code-->
<?php
session_start();
require_once 'vendor/autoload.php';
use Sani\Brands;
$brand = new Brands;
$brands = $brand->showall();
?>


    <!--Navbar-->
  <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top py-3">
    <div class="container">
    <img src="assets/image/logo.png" height="50px" alt="">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php
    $currentpage = basename($_SERVER['PHP_SELF']);
    
    ?>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li  class="nav-item">
          <a class="nav-link <?php echo $currentpage=='index.php'?'active': '' ?>" href="./index.php">Home</a>
        </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo $currentpage=='#'?'active': '' ?><?php echo $currentpage=='shop.php'?'active': '' ?><?php echo $currentpage=='shopProductDetails.php'?'active': '' ?>"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Brands Mobile
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php foreach($brands as $brand){ ?>
            <a class="dropdown-item" href="shop.php?id=<?= $brand['id' ]?>"><?= $brand['name'] ?></a><hr style="width: 100%; margin:1px; height: 1px; background-color: #DCDCDC;">
          <?php } ?>
        </div>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Brands Laptops
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item">Hp</a>
            <a class="dropdown-item">Hp</a>
            <a class="dropdown-item">Hp</a>
        </div>
      </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $currentpage=='login.php'?'active': '' ?><?php echo $currentpage=='register.php'?'active': '' ?>" href="./login.php">Login</a>
        </li>
        <li>
          <i class="fas fa-search"></i><i class="fas fa-shopping-bag"></i>
        </li>
        
      </ul>
    </div>
  </div>
  </nav>

  
</body>
</html>
