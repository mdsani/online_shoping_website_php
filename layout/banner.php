<section id="banner" class="my-5 pb-5" style="background-image: url(assets/image/homebg.jpg);">
        <div class="container">
          <h4>Sale</h4>
          <h1>Autumn collection <br>up to 20% off</h1>
          <button class="text-uppercase">Shop NOW</button>
        </div>
     </section>