

<!--footer-->
<footer class="mt-5 py-5 bg-dark">
      <div class="container">
        <div class="row">
          <div class="footer-one col-lg-4 col-md-6 col-12">
            <img src="assets/image/footer.png" height="100px" alt="">
            <p>There’s nothing wrong with a good old plain, traditional footer. If you can’t think of any specific goal you’d like to achieve through your footer, just go for the traditional approach. Promote your newsletter, include social-media buttons and use your footer for navigation and information purposes.</p>
  
          </div>
          <div class="footer-one col-lg-4 col-md-6 col-12 text-center">
            <h5 class="pb-2">Featured</h5>
            <ul class="text-uppercase list-unstyled">
              <li><a href="#">New Arrivals</a></li>
              <li><a href="#">Watch</a></li>
              <li><a href="#">men</a></li>
              <li><a href="#">Shoes</a></li>
              <li><a href="#">Cloths</a></li>
            </ul>
          </div>
          <div class="footer-one col-lg-4 col-md-6 col-12">
            <h5 class="text-uppercase">Contact us</h5>
            <div>
              <h6 class="text-uppercase">address</h6>
             <p>Uttara sector-5,Dhaka</p>
            </div>
            <div>
              <h6 class="text-uppercase">Phone</h6>
             <p>01647748746</p>
            </div>
            <div>
              <h6 class="text-uppercase">email</h6>
             <p>mdsani156@gmail.com</p>
            </div>
          </div>
        </div>
      </div>
   </footer>

 
    <!--Bootstrap js-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!--main js-->
    <script src="assets/js/js.js"></script>
</body>
</html>