<!--Header section-->
<?php
include 'layout/header.php';
?>

<!--php code-->
<?php
require_once 'vendor/autoload.php';
use Sani\Brands;
$brand = new Brands;
$productobj = $brand->show($_GET['id']);
$product = $productobj['products'];

$show = $brand->showRecommand($_GET['id']);
$totalBrand = $show['product_count'];
?>
    

      <section id="cloths">
        <div class=" container mt- py-5">
          <h2>Shop</h2>
          <hr>
          <p>This is our product</p>
        </div>

        <!--cart section-->
        <div class="row mx-auto container">
            <?php foreach($product as $products){ ?>
                <div onclick="window.location.href='shopProductDetails.php?id=<?= $products['id'] ?>';" class="product text-center col-lg-3 col-md-4 col-12" style="height: 400px;">
                <img src="assets/image/product/<?=  $products['image'] ?>" style="height: 200px; width:100%;" class="mb-5 mt-3" alt="">
              <h5> <?= $products['title'] ?></h5>
              <h4>$<?= $products['price'] ?></h4>
              <button class="buy-btn">Buy Now</button>
            </div>
            <?php } ?>
        </div>

    
        
<!--pagination-->
<div class="container">
<?php $totalPage = ceil($totalBrand/Brands::PAGINATE_PER_PAGE); 
for($i= 1; $i<=$totalPage; $i++){ ?>

<a style="border: 1px solid #DCDCDC; padding:10px; " href="index.php?page=<?= $i ?>"><?= $i ?></a>

<?php } ?>
</div>




<!--footer section-->
<?php
include 'layout/footer.php';
?>
